import "./Room";

// A Room value is exactly one of these four strings.
// It is impossible for a Room variable to contain any other string.

function getNonEmptyInput(text: string): string {
  console.warn(text);
  let input: string | null = null;
  while (input == null || input == "") {
    input = prompt(text);
    if (input == null || input == "") {
      console.error("Invalid input.");
    }
  }
  console.log(input);
  return input;
}

export function play(): void {
  console.info(
    "Welcome to the text adventure! Open your browser's developer console to play."
  );

  let playerName: string | null = getNonEmptyInput("Please enter your name.");

  console.info(
    [
      "Hello, " + playerName + ".",
      "You are in a building. Your goal is to exit this building.",
      "You are in an empty room. There are doors on the north and west walls of this room.",
    ].join("\n")
  );

  let currentRoom: Room = new RoomA();
  let hasKey: boolean = false;
  let windowOpen: boolean = false;

  while (!(currentRoom instanceof Exit)) {
    let command: string = getNonEmptyInput("Please enter a command");

    let f: Function = currentRoom.handleCommand(command);
    f();
  }

  console.info(
    [
      "You have exited the building. You win!",
      "Congratulations, " + playerName + "!",
    ].join("\n")
  );
}
