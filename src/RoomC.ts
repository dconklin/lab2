class RoomC extends Room {
    north = null;
    south = () => {
        console.info(
            "You are in an empty room. There are doors on the north and west walls of this room."
        );
    }
    east = (currentRoom: Room, windowOpen: boolean) => {
        if (windowOpen) {
            currentRoom = new Exit();
            console.info("You step out from the open window.");
        } else {
            console.error("The window is closed.");
        }
    }
    west = null;
    f = {
        "open window": (windowOpen: boolean) => {
            if (windowOpen) {
                console.error("The window is already open.");
            } else {
                console.info("You open the window.");
                windowOpen = true;
            }
        },
    }
}