class RoomB extends Room {
    north = null;
    south = null;
    east = (currentRoom: Room) => {
        currentRoom = new RoomA;
        console.info(
            "You are in an empty room. There are doors on the north and west walls of this room."
        );
    }
    west = null;
    f = {
        "take key": (hasKey: boolean) => {
            if (hasKey) {
                console.error("You already have the key.");
            } else {
                console.info("You take the key from the table.");
                hasKey = true;
            }
        }
    }
}