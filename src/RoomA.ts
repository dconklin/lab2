class RoomA extends Room {
    north = (currentRoom: Room, hasKey: boolean) => {
        if (hasKey) {
            currentRoom = new RoomC();
            console.info(
                "You unlock the north door with the key and go through the door."
            );
            console.info(
                "You are in a bright room. There is a door on the south wall of this room and a window on the east wall."
            );
        } else {
            console.error("You try to open the north door, but it is locked.");
        }
    }
    south = null;
    east = null;
    west = (currentRoom: Room, hasKey: boolean) => {
        currentRoom = new RoomB();
        console.info(
            "You go through the west door. You are in a room with a table."
        );
        if (!hasKey) {
            console.info("On the table there is a key.");
        }
        console.info("There is a door on the east wall of this room.");
    }
    f = {
    }
}