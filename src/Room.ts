abstract class Room {
    abstract north: Function | null;
    abstract south: Function | null;
    abstract east: Function | null;
    abstract west: Function | null;

    fallback() {
        console.error("Unrecognized command.");
    }

    abstract f: Record<string, Function | null>

    handleCommand(command: string): Function {
        let cmd = this.f[command];
        if (command == "north" && this.north) {
            cmd = this.north();
        } else if (command == "south" && this.south) {
            cmd = this.south();
        } else if (command == "east" && this.east) {
            cmd = this.east();
        } else if (command == "west" && this.west) {
            cmd = this.west();
        }
        return cmd ? cmd : this.fallback;
    }
}